### See the notebook. 

### Here's a binder link [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/rdubwiley%2Fpersonal_chess_analytics/HEAD)

### If you use binder you'll have to upload stockfish and do the following
1. Create a new folder called stockfish
2. Upload your stockfish binary in that folder
3. run chmod +x at your stockfish binary location (using the terminal)
4. Make sure to check the STOCKFISH_DIR paratmeter to ensure it matches the correct directories